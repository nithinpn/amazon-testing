package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

public class SearchPageObject extends BasePage {

    private @FindBy(how = How.XPATH, using = "//input[@name='field-keywords']")
    WebElement productName;

    private @FindBy(id = "nav-search-submit-button")
    WebElement searchButton;

    private @FindBy(how = How.XPATH, using = ".//span[@class='a-size-medium a-color-base a-text-normal']")
    List<WebElement> items;

    private @FindBy(id = "add-to-cart-button")
    WebElement addToCartButton;

    private @FindBy(id = "nav-cart-count")
    WebElement cartCount;

    private @FindBy(id = "nav-cart-count-container")
    WebElement goToCartButton;

    private @FindBy(id = "quantity")
    WebElement increaseQuantity;

    private @FindBy(how = How.XPATH, using = "//span[@class='a-size-medium a-color-base sc-price sc-white-space-nowrap sc-product-price a-text-bold']")
    WebElement originalPriceElement;

    private @FindBy(id = "sc-subtotal-amount-buybox")
    WebElement updatedPriceElement;


    public void navigateToHomePage() {
        navigateToUrl("https://www.amazon.in/");
    }

    public void setProductName(String product) {
        sendKeys(productName, product);
    }

    public void clickSearchButton() {
        waitForWebElementAndClick(searchButton);
    }

    public void selectThirdProduct() {
        items.get(0).click();
    }

    public void clickAddToCartButton() {
        waitForWebElementAndClick(addToCartButton);
    }

    public void validateProductCount() {

        waitForVisibilityOfElement(cartCount);
        Assert.assertEquals(cartCount.getText(), "1");

    }

    public void clickGoToCartButton() {
        waitForWebElementAndClick(goToCartButton);
    }

    public void setIncreaseQuantity() {
        Select select = new Select(increaseQuantity);
        int currentQuantity = Integer.parseInt(select.getFirstSelectedOption().getText());
        select.selectByIndex(currentQuantity + 1);
    }

    public void validatingUpdatedPrice() throws InterruptedException {
        String originalPrice = originalPriceElement.getText().replace(" ", "").replace(",", "");
        double price = Double.parseDouble(originalPrice);
        double expected = price * 2; // Doubling the price of one headphone

        Thread.sleep(5000);
        String updatedPrice = updatedPriceElement.getText().replace(" ", "").replace(",", "");
        double newPrice = Double.parseDouble(updatedPrice);
        Assert.assertEquals(newPrice, expected); //Asserting the expected price with the new price
    }

}
