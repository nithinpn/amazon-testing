package driver;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DriverFactory {
    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();

    public static WebDriver getDriver() {
        if (webDriver.get() == null) {
            webDriver.set(createDriver());
        }
        return webDriver.get();
    }

    private static WebDriver createDriver() {
        WebDriver driver = null;
        switch (getBrowserType()) {
            case "chrome" -> {
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
                        + "/src/main/java/driver/drivers/chromedriver.exe");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setPageLoadStrategy((PageLoadStrategy.NORMAL));
                driver = new ChromeDriver(chromeOptions);
                break;
            }
            case "edge" -> {
                System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")
                        + "/src/main/java/driver/drivers/msedgedriver.exe");
                EdgeOptions edgeOptions = new EdgeOptions();
                edgeOptions.setPageLoadStrategy((PageLoadStrategy.NORMAL));
                driver = new EdgeDriver(edgeOptions);
                break;
            }
        }
        driver.manage().window().maximize();
        return driver;
    }

    private static String getBrowserType() {
        String browserType = null;
        String browserTypeRemote = System.getProperty("browserType");
        try {
            if (browserTypeRemote ==null || browserTypeRemote.isEmpty()){

                Properties properties = new Properties();
                FileInputStream fileInputStream = new FileInputStream(System.getProperty("user.dir") +
                        "/src/main/java/properties/config.properties");
                properties.load(fileInputStream);
                browserType = properties.getProperty("browser").toLowerCase().trim();
            }else {
                browserType = browserTypeRemote;
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return browserType;
    }

    public static void cleanUpDriver() {
        webDriver.get().quit();
        webDriver.remove();
    }
}
