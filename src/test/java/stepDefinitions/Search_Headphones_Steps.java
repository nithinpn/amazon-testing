

package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageObjects.BasePage;
import pageObjects.SearchPageObject;

import java.util.Set;

/**
 * Step definition file for searching and adding headphone to cart
 */
public class Search_Headphones_Steps extends BasePage {

    private final WebDriver driver = getDriver();
    private final SearchPageObject searchPageObject;

    public Search_Headphones_Steps(SearchPageObject searchPageObject) {
        this.searchPageObject = searchPageObject;
    }

    @Given("I access the page")
    public void i_access_the_page() {
        searchPageObject.navigateToHomePage();
    }

    @When("I enter product name {word}")
    public void i_enter_product_name(String product) {
        searchPageObject.setProductName(product);
    }

    @And("I click on search button")
    public void i_click_on_search_button() {
        searchPageObject.clickSearchButton();
    }

    @And("I click on the third product")
    public void i_click_on_the_third_product() {
        searchPageObject.selectThirdProduct();
    }

    @And("I click on the add to cart in the page")
    public void i_click_on_the_add_to_cart_in_the_page() {

        String current = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();
        for (String actual : handles) {
            if (!actual.equalsIgnoreCase(current)) {
                driver.switchTo().window(actual);
            }
        }
        searchPageObject.clickAddToCartButton();
    }

    @And("I should see the Selected product in the cart")
    public void i_should_see_the_selected_product_in_the_cart() {
        searchPageObject.validateProductCount();
    }

    @And("I go to the cart page")
    public void i_go_to_the_cart_page() {
        searchPageObject.clickGoToCartButton();
    }

    @And("I increase the quantity of the headphone")
    public void i_increase_the_quantity_of_the_headphone() {
        searchPageObject.setIncreaseQuantity();
    }

    @Then("I should see the updated price of the item updated in the cart")
    public void i_should_see_the_updated_price_of_the_item_updated_in_the_cart() throws InterruptedException {
        searchPageObject.validatingUpdatedPrice();

    }
}


