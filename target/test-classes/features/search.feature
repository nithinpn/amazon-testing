@search
Feature:  Amazon website - Searching of product and add to cart it

  Scenario:Search and add to cart the headphone on amazon website

    Given I access the page
    When I enter product name headphone
    And I click on search button
    And I click on the third product
    And I click on the add to cart in the page
    And I should see the Selected product in the cart
    And I go to the cart page
    And I increase the quantity of the headphone
    Then I should see the updated price of the item updated in the cart